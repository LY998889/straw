package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.dto.PostQuestionDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserQuestionTests {
    @Autowired
    IQuestionService service;

    @Test
    void post(){
        Integer[] tagIds = {1,4,6};
        Integer[] teacherIds = {3,5,8};
        PostQuestionDTO postQuestionDTO = new PostQuestionDTO();
        postQuestionDTO.setTitle("测试问题");
        postQuestionDTO.setContent("如何能进入到大厂");
        postQuestionDTO.setTagIds(tagIds);
        postQuestionDTO.setTeacherIds(teacherIds);

        service.post(postQuestionDTO,5,"小王同志");
    }
}
