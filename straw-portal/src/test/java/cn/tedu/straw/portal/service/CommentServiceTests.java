package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.dto.PostCommentDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.parameters.P;

@SpringBootTest
public class CommentServiceTests {

    @Autowired
    ICommentService service;

    @Test
    void post(){
        PostCommentDTO postCommentDTO = new PostCommentDTO();
        postCommentDTO.setAnswerId(2);
        postCommentDTO.setContent("测试评论");

        service.post(postCommentDTO,22,"大魔王");
    }
}
