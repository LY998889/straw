package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.vo.TagVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TagServiceTests {

    @Autowired
    ITagService service;

    @Test
    void getTagList(){
        List<TagVo> tagList = service.getTagList();
        for (TagVo tagVo : tagList) {
            System.out.println(tagVo);
        }
    }
}
