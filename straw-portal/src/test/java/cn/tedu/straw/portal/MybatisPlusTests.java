package cn.tedu.straw.portal;

import cn.tedu.straw.portal.mapper.UserMapper;
import cn.tedu.straw.portal.model.User;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@Slf4j
public class MybatisPlusTests {

    @Autowired
    UserMapper userMapper;

    @Test
    void insert() {
        User user = new User();
        user.setUsername("程老师");
        user.setPassword("1234");
        user.setPhone("13800138008");
        int rows = userMapper.insert(user);
        System.out.println("rows = " + rows);
        log.debug("rows = " + rows);
    }

    @Test
    void deleteById() {
        Integer id = 1;
        int rows = userMapper.deleteById(id);
        System.out.println("rows = " + rows);

        log.trace("输出跟踪信息");
        log.debug("输出调试信息");
        log.info("输出一般信息");
        log.warn("输出警告信息");
        log.error("输出错误信息");

        log.debug("今天是{}年{}月{}日", 2020, 11, 11);
    }

    @Test
    void updateById() {
        User user = new User();
        user.setId(2);
        user.setNickName("怪兽终结者");
        int rows = userMapper.updateById(user);
        System.out.println("rows = " + rows);
    }

    @Test
    void selectById() {
        Integer id = 2;
        User user = userMapper.selectById(id);
        System.out.println("User >>> " + user);
    }

    @Test
    void selectCount() {
        Integer count = userMapper.selectCount(null);
        System.out.println("Count = " + count);
    }

    @Test
    void selectList() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        // queryWrapper.eq("id", 3); // id=3，eq表示“等于”，equals
        queryWrapper.gt("id", 2); // id>2, gt表示“大于”，greater than
        List<User> users = userMapper.selectList(queryWrapper);
        System.out.println("User Count = " + users.size());
        for (User user : users) {
            System.out.println(">>> " + user);
        }
    }

}
