package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.dto.StudentRegisterDTO;
import cn.tedu.straw.portal.service.impl.UserServiceImpl;
import cn.tedu.straw.portal.vo.TeacherVo;
import cn.tedu.straw.portal.vo.UserLoginVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserServiceTests {

    @Autowired
    IUserService service;

    @Test
    void registerStudent() {

            StudentRegisterDTO student = new StudentRegisterDTO();
            student.setInviteCode("JSD2007-666666");
            student.setNickName("蜡笔小新");
            student.setPassword("1234");
            student.setPhone("13900139001");
            service.registerStudent(student);
            System.out.println("注册成功！");
    }

    @Test
    void findVoByPhone(){
        String phone = "13131348224";
        UserLoginVo loginVo = service.loginByPhone(phone);
        System.out.println(loginVo);
    }

    @Test
    void getTeacherList(){
        List<TeacherVo> teacherList = service.getTeacherList();
        for (TeacherVo teacherVo : teacherList) {
            System.out.println(teacherVo);
        }
    }
}
