package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.vo.QuestionDetailVO;
import cn.tedu.straw.portal.vo.QuestionListItemVO;
import cn.tedu.straw.portal.vo.QuestionSimpleVO;
import com.github.pagehelper.PageInfo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
public class QuestionServiceTests {

    @Resource
    IQuestionService service;

    @Test
    void getMostHitsList(){
        List<QuestionSimpleVO> simpleVOList = service.getMostHitsList();
        for (QuestionSimpleVO questionSimpleVO : simpleVOList) {
            System.out.println(questionSimpleVO);
        }
    }

    @Test
    void findByUserIdQuestion(){
        PageInfo<QuestionListItemVO> question = service.getByUserIdQuestion(1, 1);
        System.out.println(question);
    }

    @Test
    void getByQuestionId(){
        QuestionDetailVO detailVO = service.getByQuestionId(70);
        System.out.println(detailVO);
    }

}
