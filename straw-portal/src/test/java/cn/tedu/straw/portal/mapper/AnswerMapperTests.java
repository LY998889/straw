package cn.tedu.straw.portal.mapper;

import cn.tedu.straw.portal.vo.AnswerListItemVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AnswerMapperTests {

    @Autowired
    AnswerMapper mapper;

    @Test
    void findByQuestionId(){
        List<AnswerListItemVO> listItemVOS = mapper.findByQuestionId(19);
        for (AnswerListItemVO listItemVO : listItemVOS) {
            System.out.println(listItemVO);
        }
    }
}
