package cn.tedu.straw.portal.mapper;

import cn.tedu.straw.portal.vo.TagVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TagMapperTests {

    @Autowired
    TagMapper mapper;

    @Test
    void findAll(){
        List<TagVo> tagVos = mapper.findAll();
        for (TagVo tagVo : tagVos) {
            System.out.println(tagVo);
        }
    }
}
