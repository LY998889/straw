package cn.tedu.straw.portal.mapper;

import cn.tedu.straw.portal.model.User;
import cn.tedu.straw.portal.vo.TeacherVo;
import cn.tedu.straw.portal.vo.UserLoginVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserMapperTests {

    @Autowired
    UserMapper mapper;

    @Test
    void findByPhone() {
        String phone = "13800138001";
        User user = mapper.findByPhone(phone);
        System.out.println("User = " + user);
    }

    @Test
    void findVoByPhone(){
        String phone = "13131348224";
        UserLoginVo loginVo = mapper.findVoByPhone(phone);
        System.out.println(loginVo);
    }

    @Test
    void findByTeachers(){
        List<TeacherVo> teachers = mapper.findByTeachers();
        for (TeacherVo teacher : teachers) {
            System.out.println(teacher);
        }
    }

}
