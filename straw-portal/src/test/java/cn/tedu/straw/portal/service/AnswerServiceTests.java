package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.dto.PostAnswerDTO;
import cn.tedu.straw.portal.model.Answer;
import cn.tedu.straw.portal.vo.AnswerListItemVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.parameters.P;

import java.util.List;

@SpringBootTest
public class AnswerServiceTests {

    @Autowired
    IAnswerService service;

    @Test
    void post(){
        PostAnswerDTO postAnswerDTO = new PostAnswerDTO();
        postAnswerDTO.setQuestionId(2);
        postAnswerDTO.setContent("测试答案正文");

        Integer userId = 12;
        String userNickName = "张三";

        service.post(postAnswerDTO,userId,userNickName);
    }

    @Test
    void getAnswerList(){
        List<AnswerListItemVO> answerList = service.getAnswerList(19);
        for (AnswerListItemVO answerListItemVO : answerList) {
            System.out.println(answerList);
        }
    }
}
