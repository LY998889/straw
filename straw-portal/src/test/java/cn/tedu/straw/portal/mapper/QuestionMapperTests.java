package cn.tedu.straw.portal.mapper;

import cn.tedu.straw.portal.vo.QuestionDetailVO;
import cn.tedu.straw.portal.vo.QuestionListItemVO;
import cn.tedu.straw.portal.vo.QuestionSimpleVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class QuestionMapperTests {

    @Autowired
    QuestionMapper mapper;

    @Test
    void findMostHits(){
        List<QuestionSimpleVO> questions = mapper.findMostHits();
        System.out.println(questions.size());
        for (QuestionSimpleVO question : questions) {
            System.out.println(question);
        }
    }

    @Test
    void findByUserId(){
        //查询第几页?
        Integer pagNum = 1;
        //分页时最多几条数据
        Integer pageSize = 10;
        //应用分页参数
        PageHelper.startPage(pagNum,pageSize);

        List<QuestionListItemVO> itemVOS = mapper.findByUserId(1);
        PageInfo<QuestionListItemVO> pageInfo = new PageInfo<>(itemVOS);
        System.out.println(pageInfo);
        //System.out.println(itemVOS.size());
//        for (QuestionListItemVO itemVO : itemVOS) {
//            System.out.println(itemVO);
//        }
    }

    @Test
    void findByQuestionId(){
        int questionId = 5;
        QuestionDetailVO detailVO = mapper.findByQuestionId(questionId);
        System.out.println(detailVO);
    }
}
