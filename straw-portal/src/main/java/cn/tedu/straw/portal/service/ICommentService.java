package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.dto.PostCommentDTO;
import cn.tedu.straw.portal.ex.StrawException;
import cn.tedu.straw.portal.model.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
public interface ICommentService extends IService<Comment> {

    /**
     * 发表评论
     * @param postCommentDTO 接收参数实体类
     * @param userId 用户ID
     * @param userNickName 用户昵称
     */
    void post(PostCommentDTO postCommentDTO, Integer userId,String userNickName);
}
