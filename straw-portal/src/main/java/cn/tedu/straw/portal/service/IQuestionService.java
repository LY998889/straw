package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.dto.PostQuestionDTO;
import cn.tedu.straw.portal.model.Question;
import cn.tedu.straw.portal.vo.QuestionDetailVO;
import cn.tedu.straw.portal.vo.QuestionListItemVO;
import cn.tedu.straw.portal.vo.QuestionSimpleVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import io.swagger.models.auth.In;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
public interface IQuestionService extends IService<Question> {

    /**
     * 发表问题
     * @param postQuestionDTO 问题实体类
     * @param userId 用户id
     * @param nickName 用户昵称
     */
    void post(PostQuestionDTO postQuestionDTO, Integer userId, String nickName);

    /**
     * 热点问题
     * @return 热点问题集合
     */
    List<QuestionSimpleVO> getMostHitsList();

    /**
     * 我的问答
     * @param userId 当前用户ID
     * @return 我的问答集合
     */
    PageInfo<QuestionListItemVO> getByUserIdQuestion(Integer pageNum,Integer userId);

    /**
     * 问题详情接口
     * @param questionId 问题ID
     * @return 具体问题详情
     */
    QuestionDetailVO getByQuestionId(Integer questionId);
}
