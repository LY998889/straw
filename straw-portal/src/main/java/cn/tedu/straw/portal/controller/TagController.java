package cn.tedu.straw.portal.controller;


import cn.tedu.straw.portal.result.Result;
import cn.tedu.straw.portal.service.ITagService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
@Api(tags = "标签接口")
@RestController
@RequestMapping("/portal/tag")
public class TagController {

    @Autowired
    private ITagService tagService;

    @GetMapping("list")
    public Result getTagList(){
        return Result.ok(tagService.getTagList());
    }

}
