package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.dto.PostAnswerDTO;
import cn.tedu.straw.portal.model.Answer;
import cn.tedu.straw.portal.vo.AnswerListItemVO;
import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
public interface IAnswerService extends IService<Answer> {

    /**
     * 写答案接口
     * @param postAnswerDTO 答案实体类
     * @param userId 用户ID
     * @param userNickName 用户昵称
     */
    void post(PostAnswerDTO postAnswerDTO,Integer userId,String userNickName);

    /**
     * 显示答案以及对应评论
     * @param questionId 问题ID
     * @return 答案与评论集合
     */
    List<AnswerListItemVO> getAnswerList(Integer questionId);
}
