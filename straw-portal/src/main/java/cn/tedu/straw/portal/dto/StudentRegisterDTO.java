package cn.tedu.straw.portal.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class StudentRegisterDTO implements Serializable {

    @NotNull(message = "必须提交邀请码才能注册")
    private String inviteCode;
    private String phone;
    @Size(min = 2,max = 6,message = "昵称必须是2-6位")
    private String nickName;
    private String password;
}
