package cn.tedu.straw.portal.service.impl;

import cn.tedu.straw.portal.dto.PostQuestionDTO;
import cn.tedu.straw.portal.ex.StrawException;
import cn.tedu.straw.portal.mapper.QuestionMapper;
import cn.tedu.straw.portal.mapper.QuestionTagMapper;
import cn.tedu.straw.portal.mapper.UserQuestionMapper;
import cn.tedu.straw.portal.model.Question;
import cn.tedu.straw.portal.model.QuestionTag;
import cn.tedu.straw.portal.model.UserQuestion;
import cn.tedu.straw.portal.result.ResultCodeEnum;
import cn.tedu.straw.portal.service.IQuestionService;
import cn.tedu.straw.portal.vo.QuestionDetailVO;
import cn.tedu.straw.portal.vo.QuestionListItemVO;
import cn.tedu.straw.portal.vo.QuestionSimpleVO;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
@Service
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Question> implements IQuestionService {

    @Autowired
    QuestionMapper questionMapper;
    @Autowired
    QuestionTagMapper questionTagMapper;
    @Autowired
    UserQuestionMapper userQuestionMapper;

    @Transactional
    @Override
    public void post(PostQuestionDTO postQuestionDTO, Integer userId, String nickName) {

        LocalDateTime now = LocalDateTime.now();

        /*向question表中插入数据*/
        Question question = new Question();
        question.setTitle(postQuestionDTO.getTitle());
        question.setContent(postQuestionDTO.getContent());
        question.setUserId(userId);
        question.setUserNickName(nickName);
        question.setStatus(0);
        question.setHits(0);
        question.setTagIds(Arrays.toString(postQuestionDTO.getTagIds()));
        question.setGmtCreate(now);
        question.setGmtModified(now);

        int rows = questionMapper.insert(question);
        if (rows != 1){
            throw new StrawException("发表问题失败!请稍后再试",606);
        }

        // 遍历参数tagIds
        /*向question_tag表中插入数据*/
        for (Integer tagId : postQuestionDTO.getTagIds()) {
            QuestionTag questionTag = new QuestionTag();
            questionTag.setQuestionId(question.getId());
            questionTag.setTagId(tagId);
            questionTag.setGmtCreate(now);
            questionTag.setGmtModified(now);
            rows = questionTagMapper.insert(questionTag);
            if (rows != 1){
                throw new StrawException("发表问题失败!请稍后再试",606);
            }
        }

        //遍历teacherIds
        /*向user_question表中插入数据*/
        for (Integer teacherId : postQuestionDTO.getTeacherIds()) {
            UserQuestion userQuestion = new UserQuestion();
            userQuestion.setQuestionId(question.getId());
            userQuestion.setUserId(teacherId);
            userQuestion.setGmtCreate(now);
            userQuestion.setGmtModified(now);
            rows = userQuestionMapper.insert(userQuestion);
            if (rows != 1){
                throw new StrawException("发表问题失败!请稍后再试",606);
            }
        }
    }

    @Override
    public List<QuestionSimpleVO> getMostHitsList() {
        return questionMapper.findMostHits();
    }

    @Value("${project.my-questions.page-size}")
    int pageSize;
    @Override
    public PageInfo<QuestionListItemVO> getByUserIdQuestion(Integer pageNum,Integer userId) {
        if (pageNum == null || pageNum == 0){
            pageNum = 1;
        }
        PageHelper.startPage(pageNum,pageSize);
        List<QuestionListItemVO> questions = questionMapper.findByUserId(userId);
        PageInfo<QuestionListItemVO> pageInfo = new PageInfo<>(questions);
        return pageInfo;
    }

    @Override
    public QuestionDetailVO getByQuestionId(Integer questionId) {
        QuestionDetailVO question = questionMapper.findByQuestionId(questionId);
        if (question == null){
            throw new StrawException(ResultCodeEnum.QUESTION_NOT_FOUND_ERROR);
        }
        return question;
    }

}
