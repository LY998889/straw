package cn.tedu.straw.portal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HtmlController {
    @GetMapping("/index.html")
    public String index() {
        return "index"; // /templates/ + index + .html > /templates/index.html
    }

    @GetMapping("/login.html")
    public String login() {
        return "login";
    }

    @GetMapping("/register.html")
    public String register() {
        return "register";
    }

    @GetMapping("/question/create.html")
    public String createQuestion() {
        return "question/create";
    }

    @GetMapping("/question/detail.html")
    public String QuestionDetail() {
        return "question/detail";
    }


}