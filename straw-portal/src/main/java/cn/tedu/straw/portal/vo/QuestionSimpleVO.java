package cn.tedu.straw.portal.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class QuestionSimpleVO implements Serializable {
    private Integer id;
    private String title;
    private Integer status;
    private Integer hits;
    private Integer count;
}
