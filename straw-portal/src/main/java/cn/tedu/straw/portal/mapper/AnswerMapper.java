package cn.tedu.straw.portal.mapper;

import cn.tedu.straw.portal.model.Answer;
import cn.tedu.straw.portal.vo.AnswerListItemVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
@Repository
public interface AnswerMapper extends BaseMapper<Answer> {

    /**
     * 显示答案以及评论
     * @return 答案与对应评论集合
     */
    List<AnswerListItemVO> findByQuestionId(Integer questionId);
}
