package cn.tedu.straw.portal.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class TagVo implements Serializable {

    private Integer id;
    private String name;
}
