package cn.tedu.straw.portal.ex;

import cn.tedu.straw.portal.result.ResultCodeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 自定义全局异常类
 */
@Data
@ApiModel(value = "自定义全局异常类")
public class StrawException extends RuntimeException {

    @ApiModelProperty(value = "异常状态码")
    private Integer code;

    /**
     * 通过状态码和错误消息创建异常对象
     * @param message 错误消息
     * @param code 异常状态码
     */
    public StrawException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public StrawException(String message) {
        super(message);
    }

    /**
     * 接收枚举类型对象
     * @param resultCodeEnum 状态枚举
     */
    public StrawException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
    }

    @Override
    public String toString() {
        return "StrawException{" +
                "code=" + code +
                ", message=" + this.getMessage() +
                '}';
    }
}
