package cn.tedu.straw.portal.controller;


import cn.tedu.straw.portal.dto.PostQuestionDTO;
import cn.tedu.straw.portal.result.Result;
import cn.tedu.straw.portal.security.LoginUserInfo;
import cn.tedu.straw.portal.service.IUserQuestionService;
import cn.tedu.straw.portal.service.IUserService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */

@RestController
@RequestMapping("/portal/userQuestion")
@Slf4j
public class UserQuestionController {

}
