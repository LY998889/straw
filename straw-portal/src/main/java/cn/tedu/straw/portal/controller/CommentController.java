package cn.tedu.straw.portal.controller;


import cn.tedu.straw.portal.dto.PostCommentDTO;
import cn.tedu.straw.portal.ex.StrawException;
import cn.tedu.straw.portal.result.Result;
import cn.tedu.straw.portal.security.LoginUserInfo;
import cn.tedu.straw.portal.service.ICommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
@Api(tags = "评论相关接口")
@RestController
@RequestMapping("/portal/comment")
@Slf4j
public class CommentController {

    @Autowired
    private ICommentService commentService;

    @ApiOperation(value = "发表评论")
    @PostMapping("/post")
    public Result<Void> postAnswer(@Validated PostCommentDTO postCommentDTO, BindingResult bindingResult,
                                   @AuthenticationPrincipal LoginUserInfo loginUserInfo){

        log.debug("发表评论,客户端提交的参数: {}",postCommentDTO);
        log.debug("发表评论,当前评论用户: {}",loginUserInfo);

        if (bindingResult.hasErrors()){
            String message = bindingResult.getFieldError().getDefaultMessage();
            throw new StrawException(message);
        }

        Integer userId = loginUserInfo.getUserId();
        String nickName = loginUserInfo.getNickName();

        commentService.post(postCommentDTO,userId,nickName);

        return Result.ok();
    }


}
