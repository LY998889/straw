package cn.tedu.straw.portal.service.impl;

import cn.tedu.straw.portal.dto.StudentRegisterDTO;
import cn.tedu.straw.portal.ex.StrawException;
import cn.tedu.straw.portal.mapper.ClassInfoMapper;
import cn.tedu.straw.portal.mapper.UserMapper;
import cn.tedu.straw.portal.mapper.UserRoleMapper;
import cn.tedu.straw.portal.model.ClassInfo;
import cn.tedu.straw.portal.model.User;
import cn.tedu.straw.portal.model.UserRole;
import cn.tedu.straw.portal.result.Result;
import cn.tedu.straw.portal.result.ResultCodeEnum;
import cn.tedu.straw.portal.service.IUserService;
import cn.tedu.straw.portal.utils.PasswordUtils;
import cn.tedu.straw.portal.vo.TeacherVo;
import cn.tedu.straw.portal.vo.UserLoginVo;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    ClassInfoMapper classInfoMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserRoleMapper userRoleMapper;

    @Override
    @Transactional
    public void registerStudent(StudentRegisterDTO studentRegisterDTO) {
        // 基于参数inviteCode调用classInfoMapper.findByInviteCode()执行查询
        ClassInfo classInfo = classInfoMapper.findByInviteCode(studentRegisterDTO.getInviteCode());
        // 判断查询结果是否为null
        if (classInfo == null) {
            // 是：抛出InviteCodeException
            throw  new StrawException(ResultCodeEnum.INVITE_CODE_ERROR);
        }

        // 判断以上查询结果中的isEnabled是否为0
        if (classInfo.getIsEnabled() == 0) {
            // 是：抛出ClassDisabledException
            throw new StrawException(ResultCodeEnum.CLASS_DISABLED_ERROR);
        }

        // 基于参数phone调用userMapper.findByPhone()执行查询
        User result = userMapper.findByPhone(studentRegisterDTO.getPhone());
        // 判断查询结果是否不为null
        if (result != null) {
            // 是：抛出PhoneDuplicateException
            throw new StrawException(ResultCodeEnum.PHONE_DUPLICATE_ERROR);
        }

        // 创建当前时间对象now
        LocalDateTime now = LocalDateTime.now();
        // 创建User对象
        User user = new User();
        // 补全user对象的属性值：nickName < 参数nickName
        user.setNickName(studentRegisterDTO.getNickName());
        // 补全user对象的属性值：password < 参数password
        user.setPassword(PasswordUtils.encode(studentRegisterDTO.getPassword()));
        // 补全user对象的属性值：phone < 参数phone
        user.setPhone(studentRegisterDTO.getPhone());
        // 补全user对象的属性值：classId < 此前的查询结果
        user.setClassId(classInfo.getId());
        // 补全user对象的属性值：isEnabled < 1
        user.setIsEnabled(1);
        // 补全user对象的属性值：isLocked < 0
        user.setIsLocked(0);
        // 补全user对象的属性值：accountType < 0
        user.setAccountType(0);
        // 补全user对象的属性值：gmtCreate < now
        user.setGmtCreate(now);
        // 补全user对象的属性值：gmtModified < now
        user.setGmtModified(now);
        // 基于以上user对象调用userMapper.insert()执行插入数据，并获取返回的“受影响行数”
        int rows = userMapper.insert(user);
        // 判断以上返回的“受影响行数”是否不为1
        if (rows != 1) {
            // 是：抛出InsertException
           throw new StrawException(ResultCodeEnum.INSERT_ERROR);
        }

        //向User_Role表中插入数据,为当前注册的账号分配"学生"角色
        UserRole userRole = new UserRole();
        userRole.setUserId(user.getId());
        userRole.setRoleId(2);
        userRole.setGmtCreate(now);
        userRole.setGmtModified(now);
        rows = userRoleMapper.insert(userRole);
        if (rows != 1){
            throw new StrawException(ResultCodeEnum.INVITE_CODE_ERROR);
        }
    }

    @Override
    public UserLoginVo loginByPhone(String phone) {
        return userMapper.findVoByPhone(phone);
    }

    @Override
    public List<TeacherVo> getTeacherList() {
        return userMapper.findByTeachers();
    }

}
