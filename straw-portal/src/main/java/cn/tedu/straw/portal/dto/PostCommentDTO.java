package cn.tedu.straw.portal.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class PostCommentDTO implements Serializable {
    @NotNull(message = "发表评论失败!缺少必要的参数")
    private Integer answerId;
    @NotNull(message = "发表评论失败!评论不能为空")
    @Size(min = 1,max = 65535,message = "发表评论失败!评论正文必须为1-65535个字符")
    private String content;
}
