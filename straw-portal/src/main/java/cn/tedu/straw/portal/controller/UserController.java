package cn.tedu.straw.portal.controller;


import cn.tedu.straw.portal.dto.StudentRegisterDTO;
import cn.tedu.straw.portal.ex.StrawException;
import cn.tedu.straw.portal.result.Result;
import cn.tedu.straw.portal.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
@Api(tags = "用户接口")
@RestController
@RequestMapping("/portal/user")
@Slf4j
public class UserController {

    @Autowired
    private IUserService userService;

    @ApiOperation(value = "学生注册接口")
    @PostMapping("register/Student")
    public Result registerStudent(@Validated StudentRegisterDTO studentRegisterDTO,
                                  BindingResult bindingResult){
        log.debug("StudentRegisterDTO >>> {}",studentRegisterDTO);

        if (bindingResult.hasErrors()){
            String message = bindingResult.getFieldError().getDefaultMessage();
            throw new StrawException(message,600);
        }

        userService.registerStudent(studentRegisterDTO);
        return Result.ok();
    }

    @ApiOperation(value = "查询全部教师接口")
    @GetMapping("teacherList")
    public Result getTeacherList(){

        return Result.ok(userService.getTeacherList());
    }

}
