package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.model.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
public interface IUserRoleService extends IService<UserRole> {

}
