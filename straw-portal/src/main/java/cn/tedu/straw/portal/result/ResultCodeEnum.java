package cn.tedu.straw.portal.result;

import lombok.Getter;

/**
 * 统一返回结果状态信息类
 */
@Getter
public enum ResultCodeEnum {

    SUCCESS(200,"成功"),
    FAIL(201,"失败"),
    INVITE_CODE_ERROR(400,"邀请码错误"),
    CLASS_DISABLED_ERROR(410,"班级已经被禁用"),
    PHONE_DUPLICATE_ERROR(420,"手机号已存在"),
    INSERT_ERROR(600,"注册失败！服务器忙，请稍后再次尝试！"),
    QUESTION_NOT_FOUND_ERROR(700,"获取问题详情失败!尝试访问的数据不存在,可能被删除"),
    QUESTION_NOT_FOUND_ANSWER_ERROR(800,"发布答案失败!问题不存在!可能被删除"),
    COMMENT_ERROR(900,"发表评论失败!答案不存在")
    ;

    private final Integer code;
    private final String message;

    ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
