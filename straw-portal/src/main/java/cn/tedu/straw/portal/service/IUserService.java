package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.dto.StudentRegisterDTO;
import cn.tedu.straw.portal.model.User;
import cn.tedu.straw.portal.vo.TeacherVo;
import cn.tedu.straw.portal.vo.UserLoginVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
public interface IUserService extends IService<User> {

    void registerStudent(StudentRegisterDTO studentRegisterDTO);

    /**
     * 根据手机号查询用户信息
     * @param phone 手机号
     * @return
     */
    UserLoginVo loginByPhone(String phone);

    /**
     * 返回教师集合
     * @return
     */
    List<TeacherVo> getTeacherList();
}
