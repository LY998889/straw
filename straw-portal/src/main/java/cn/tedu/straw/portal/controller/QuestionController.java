package cn.tedu.straw.portal.controller;


import cn.tedu.straw.portal.dto.PostQuestionDTO;
import cn.tedu.straw.portal.ex.StrawException;
import cn.tedu.straw.portal.result.Result;
import cn.tedu.straw.portal.result.ResultCodeEnum;
import cn.tedu.straw.portal.security.LoginUserInfo;
import cn.tedu.straw.portal.service.IQuestionService;
import cn.tedu.straw.portal.service.IUserQuestionService;
import cn.tedu.straw.portal.vo.QuestionDetailVO;
import cn.tedu.straw.portal.vo.QuestionListItemVO;
import cn.tedu.straw.portal.vo.QuestionSimpleVO;
import com.github.pagehelper.PageInfo;
import com.mysql.cj.xdevapi.InsertParams;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
@Api(tags = "问题接口")
@RestController
@RequestMapping("/portal/question")
@Slf4j
public class QuestionController {

    @Autowired
    private IQuestionService questionService;

    @ApiOperation(value = "我要提问接口")
    @PostMapping("post")
    public Result<Void> post(@Validated PostQuestionDTO postQuestionDTO, BindingResult bindingResult,
                       @AuthenticationPrincipal LoginUserInfo loginUserInfo){

        log.debug("发布问题,客户端提交的数据: {}",postQuestionDTO);
        log.debug("发布问题,当前登录用户信息: {}",loginUserInfo);

        if (bindingResult.hasErrors()){
            String message = bindingResult.getFieldError().getDefaultMessage();
            throw new StrawException(message);
        }

        Integer userId = loginUserInfo.getUserId();
        String nickName = loginUserInfo.getNickName();

        //发布问题
        questionService.post(postQuestionDTO,userId,nickName);
        return Result.ok();
    }

    @Value("${project.post-question.upload-image.base-dir}")
    String uploadImageBaseDir;
    @Value("${project.post-question.upload-image.max-file-size}")
    int maxFileSize;
    @Value("${project.post-question.upload-image.file-types}")
    public List<String> fileTypes;

    @ApiOperation(value = "上传图片接口")
    @PostMapping("image/upload")
    public Result<String> uploadImage(MultipartFile file) {

        //检查上传的文件是否为空
        if (file.isEmpty()){
            throw new StrawException("上传图片失败！请选择有效的文件！");
        }

        //判断上传的文件大小是否超出了限制
        if (file.getSize() > maxFileSize){
            throw new StrawException("上传图片失败!不允许上传超过"+maxFileSize/1024/1024+"MB的图片");
        }

        // 判断上传的文件类型是否超出了限制，允许：image/jpeg、image/png、image/bmp
        if (!fileTypes.contains(file.getContentType())){
            throw new StrawException("上传图片失败！上传的文件类型超出了限制！ 允许上传的文件类型有：" + fileTypes);
        }

        //保存文件的子级文件夹
        LocalDateTime now = LocalDateTime.now();
        String subDir = DateTimeFormatter.ofPattern("yyyy/MM").format(now);
        //保存文件的文件夹
        File parent = new File(uploadImageBaseDir,subDir);
        if (!parent.exists()){
            parent.mkdirs();
        }
        //保存文件的文件名
        String fileName = System.currentTimeMillis()+"-"+System.nanoTime();
        //保存文件的扩展名
        String originalFilename = file.getOriginalFilename();
        int beginIndex = originalFilename.lastIndexOf(".");
        String suffix = originalFilename.substring(beginIndex);
        //保存文件的文件全名
        String child = fileName+suffix;
        //执行保存
        File dest = new File(parent,child);

        try {
            file.transferTo(dest);
        }catch (IllegalStateException e){
            throw new StrawException("上传图片失败！请检查上传的文件是否有效");
        }catch (IOException e){
            throw new StrawException("上传图片失败！服务器忙，请稍后再次尝试！");
        }

        // 图片路径
        String url = "http://localhost:8080/" + subDir + "/" + child;
        System.out.println("\tURL >>> " + url);

        return Result.ok(url);

        /*
        String getOriginalFilename() ：获取原始文件名，即上传的文件在客户端设备中的名称；
        boolean isEmpty() ：判断上传的文件是否为空，如果在上传的表单中没有选择文件就直接提交，
                           或上传的文件无内容（0字节文件）将返回 true ，
                           反之，如果上传的是有效文件则返回 false；
        long getSize() ：获取上传的文件的大小，以字节为单位；
        String getContentType() ：获取文件的MIME类型，例如 image/jpeg 、 image/png ；
        void transferTo(File dest) throws IllegalStateException, IOException ：将客户端上传的文件保存为参数 dest 对应的文件。
         */
    }

    @ApiOperation(value = "热点问题接口")
    @GetMapping("mostHits")
    public Result<List<QuestionSimpleVO>> getMostHitsList(){
        return Result.ok(questionService.getMostHitsList());
    }

    @ApiOperation(value = "我的问答接口")
    @GetMapping("/my")
    public Result<PageInfo<QuestionListItemVO>> getQuestionsByUserId(Integer page,
                                                                     @AuthenticationPrincipal LoginUserInfo loginUserInfo){
        Integer userId = loginUserInfo.getUserId();
        PageInfo<QuestionListItemVO> pageInfo = questionService.getByUserIdQuestion(page, userId);
        return Result.ok(pageInfo);
    }

    @ApiOperation(value = "问题详情接口")
    @GetMapping("/{questionId}")
    public Result<QuestionDetailVO> getDetail(@PathVariable Integer questionId){
        return Result.ok(questionService.getByQuestionId(questionId));
    }

}
