package cn.tedu.straw.portal.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Permission implements Serializable {

    private String authority;
}
