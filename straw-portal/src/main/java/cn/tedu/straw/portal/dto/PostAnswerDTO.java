package cn.tedu.straw.portal.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class PostAnswerDTO implements Serializable {
    @NotNull(message = "发表答案失败!缺少必要的参数")
    private Integer questionId;
    @NotNull(message = "发表答案失败!必须提交答案正文")
    @Size(min = 2,max = 65535,message = "发表答案失败!答案正文必须是2-65535个字符")
    private String content;
}
