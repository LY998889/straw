package cn.tedu.straw.portal.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class TeacherVo implements Serializable {
    private Integer id;
    private String nickName;
}
