package cn.tedu.straw.portal.ex;

import cn.tedu.straw.portal.result.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(Exception e) {
        e.printStackTrace();
        return Result.fail();
    }

    @ExceptionHandler(StrawException.class)
    @ResponseBody
    public Result error(StrawException e) {
        e.printStackTrace();
        return Result.build(e.getCode(),e.getMessage());
    }
}