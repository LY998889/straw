package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.dto.PostQuestionDTO;
import cn.tedu.straw.portal.model.UserQuestion;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
public interface IUserQuestionService extends IService<UserQuestion> {


}
