package cn.tedu.straw.portal.mapper;

import cn.tedu.straw.portal.model.Question;
import cn.tedu.straw.portal.vo.QuestionDetailVO;
import cn.tedu.straw.portal.vo.QuestionListItemVO;
import cn.tedu.straw.portal.vo.QuestionSimpleVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
@Repository
public interface QuestionMapper extends BaseMapper<Question> {

    /**
     * 热点问题
     * @return 热点问题集合
     */
    List<QuestionSimpleVO> findMostHits();

    /**
     * 我的问答
     * @param userId 当前用户ID
     * @return 我的问答集合
     */
    List<QuestionListItemVO> findByUserId(Integer userId);

    /**
     * 问题详情
     * @param questionId 问题ID
     * @return 具体问题详情
     */
    QuestionDetailVO findByQuestionId(Integer questionId);
}
