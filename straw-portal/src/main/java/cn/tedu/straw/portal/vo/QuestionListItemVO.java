package cn.tedu.straw.portal.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class QuestionListItemVO implements Serializable {
    private Integer id;
    private String title;
    private String content;
    private String userNickName;
    private Integer status;
    private Integer hits;
    private String tagIds;
    private LocalDateTime gmtCreate;
}
