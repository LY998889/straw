package cn.tedu.straw.portal.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class QuestionDetailVO implements Serializable {
    private Integer id;
    private String title;
    private String content;
    private Integer userId;
    private String userNickName;
    private Integer status;
    private Integer hits;
    private String tagIds;
    private LocalDateTime gmtCreate;

}
