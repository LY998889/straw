package cn.tedu.straw.portal.service;

import cn.tedu.straw.portal.model.Tag;
import cn.tedu.straw.portal.vo.TagVo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
public interface ITagService extends IService<Tag> {

    /**
     * 查询标签列表
     * @return 所有标签
     */
    List<TagVo> getTagList();
}
