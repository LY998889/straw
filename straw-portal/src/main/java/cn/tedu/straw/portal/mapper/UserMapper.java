package cn.tedu.straw.portal.mapper;

import cn.tedu.straw.portal.model.User;
import cn.tedu.straw.portal.vo.TeacherVo;
import cn.tedu.straw.portal.vo.UserLoginVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tedu.cn
 * @since 2020-11-11
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    User findByPhone(String phone);

    UserLoginVo findVoByPhone(String phone);

    /**
     * 查询全部教师
     * @return 教师集合
     */
    List<TeacherVo> findByTeachers();

}
