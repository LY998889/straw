package cn.tedu.straw.portal.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class PostQuestionDTO implements Serializable {
    @NotNull(message = "发布问题失败!标题不能为空")
    @Size(min = 5,max = 100,message = "发布问题失败!标题必须为5~100个字符")
    private String title;
    @NotNull(message = "发布问题失败!问题正文不能为空")
    private String content;
    @NotNull(message = "发布问题失败!必须选择问题标签")
    private Integer[] tagIds;
    @NotNull(message = "发布问题失败!必须选择老师标签")
    private Integer[] teacherIds;
}
