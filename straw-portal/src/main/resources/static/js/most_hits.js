let mostHitsApp = new Vue({
    el:'#mostHitsApp',
    data:{
        questions:[
            {
                "id": 12,
                "title": "猪是怎么死的?",
                "status": 2,
                "statusText": "已解决",
                "statusClass": "text-success",
                "hits":73,
                "count": 12
            },{
                "id": 13,
                "title": "Servlet和Jsp什么关系?",
                "status": 0,
                "statusText": "未回复",
                "statusClass": "text-warning",
                "hits":34
            },{
                "id": 14,
                "title": "Get和Post请求的区别?",
                "status": 1,
                "statusText": "未解决",
                "statusClass": "text-info",
                "hits":232
            },
        ]
    },
    methods: {
        loadMostHits: function () {
            $.ajax({
                url: '/portal/question/mostHits',
                success: function (r) {
                    let list = r.data;
                    let questions = [];
                    let statusTexts = ['未回复','未解决','已解决'];
                    let statusClasses = ['text-warning','text-info','text-success'];
                    for (let i = 0; i < list.length; i++) {
                        let question = list[i];
                        question.statusText = statusTexts[question.status];
                        question.statusClass = statusClasses[question.status];
                        questions[i] = question;
                    }
                    mostHitsApp.questions = questions;
                }
            })
        }
    },
    created: function () {
        this.loadMostHits();
    }
});