let myQuestionApp = new Vue({
   el:'#myQuestionApp',
   data:{
     questions:[
         {
             id: 12,
             title: '猪是怎么死的?',
             status: 1,
             statusText: '未解决',
             statusClass: 'badge-info',
             userNickName: '大鲁班',
             hits:73,
             gmtCreate: '2021-11-20T10:34:09',
             gmtCreateText: '11分钟前'
         }
     ],
     navigatepageNums: [1,2,3,4,5,6,7,8],
     currentPageNum: 1,
     prePageNum: 1,
     nextPageNum: 1
   },
    methods:{
       loadMyQuestions: function (page) {
           if (page === undefined || page === "" || page < 1){
               page = 1;
           }
           $.ajax({
               url: '/portal/question/my',
               data:'page=' + page,
               success: function (r) {
                   let list = r.data.list;
                   console.log(list);
                   let questions = [];
                   let statusTexts = ['未回复','未解决','已解决'];
                   let statusClasses = ['badge-warning','badge-info','badge-success'];
                   for (let i = 0; i < list.length; i++) {
                       let question = list[i];
                       question.statusText = statusTexts[question.status];
                       question.statusClass = statusClasses[question.status];
                       question.gmtCreateText = getTimeText(question.gmtCreate);
                       questions[i] = question;
                   }
                   myQuestionApp.questions = questions;
                   myQuestionApp.navigatepageNums = r.data.navigatepageNums;
                   myQuestionApp.currentPageNum = r.data.pageNum;
                   myQuestionApp.prePageNum = r.data.prePage;
                   myQuestionApp.nextPageNum = r.data.nextPage;
               }
           })
       }
    },
    created: function () {
       this.loadMyQuestions(1);
    }
});