let tagsApp = new Vue({
    el:'#tagsApp',
    data:{
        tags: [
                { id: 1, name: 'Spring' },
                { id: 2, name: 'SpringMvc' },
                { id: 3, name: 'Mybatis' },
                { id: 4, name: 'SpringBoot' },
                { id: 5, name: 'SpringCloud' },
        ]
    },
    methods:{
        getTagList: function () {
            $.ajax({
                url: '/portal/tag/list',
                success: function (r) {
                    tagsApp.tags = r.data
                }
            })
        }
    },
    created: function () {
        this.getTagList()
    }
});