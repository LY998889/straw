let detailApp = new Vue({
   el:'#detailApp',
   data:{
       id: 0,
       question:{
           title: '测试标题',
           content: '测试正文',
           hits: '1999',
           userNickName: '恭亲王',
           gmtCreateText: '8分钟前'
       },
       answers:[
           {
               id: 5,
               userNickName: '张三',
               content: '测试答案5',
               gmtCreateText: '9分钟前'
           }
       ]
   },
   methods:{
       /*答案详情*/
       loadQuestionDetail: function () {
           //alert("准备加载问题ID="+this.id+"的详情");
           $.ajax({
              url: '/portal/question/' + this.id,
              success: function (r) {
                  if (r.code === 200){
                      //alert("获取问题详情数据成功,可以从浏览器控制台查看");
                      //console.log(r.data);
                      let question = r.data;
                      question.gmtCreateText = getTimeText(question.gmtCreate);
                      detailApp.question = question;
                  }else {
                      alert(r.message);
                      location.href = "/";
                  }
              }
           });
       },
       /*写答案*/
       postAnswer: function () {
           //alert("准备提交答案...")
           $.ajax({
              url: '/portal/answer/post',
              data:{
                  questionId: this.id,
                  content: $('#summernote').val()
              },
              type: 'post',
               success: function (r) {
                  if (r.code === 200){
                      alert("发表答案成功");
                      detailApp.loadAnswers();
                      location.href = '#answers';
                      $('#summernote').summernote('reset');
                  }else if (r.code === 701){
                      alert(r.message);
                      location.href = "/";
                  }else {
                      alert(r.message);
                  }

               }
           });
       },
       /*显示答案*/
       loadAnswers: function () {
           //alert("准备显示答案")
           $.ajax({
              url: '/portal/answer/',
              data: 'questionId=' + this.id,
              success: function (r) {
                  let list = r.data;
                  //console.log(r.data.comments.length);
                  let answers = [];
                  for (let i = 0; i < list.length; i++) {
                      let answer = list[i];
                      answer.gmtCreateText = getTimeText(answer.gmtCreate);
                      answers[i] = answer;
                  }
                  detailApp.answers = answers;
              }
           });
       },
       /*发表评论*/
       postComment: function (answerId) {
           //alert("发表评论")
           $.ajax({
              url:'/portal/comment/post',
              type: 'post',
              data:{
                  answerId: answerId,
                  content: $('#commentContent' + answerId).val(),
              },
              success: function (r) {
                if (r.code === 200){
                    alert("发表评论成功");
                    detailApp.loadAnswers();
                }else {
                    alert(r.message);
                }
              }
           });
       },

   },
    created: function () {
       let id = location.search.substring(1);
       if (id === "" || isNaN(id)){
           alert("缺少必要的参数");
           location.href = "/";
           return;
       }

       this.id = id;
       this.loadQuestionDetail();
       this.loadAnswers();
    }

});