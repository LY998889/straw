Vue.component('v-select',VueSelect.VueSelect);
let postQuestionApp = new Vue({
   el:'#postQuestionApp',
   data:{
       //注意：Vue-Select默认使用的选项名称是数据的 label 属性，提交的值是 value 属性。
       /*问题标签数组*/
       tags:[
           { value: 1, label: 'Spring' },
           { value: 2, label: 'SpringMvc' },
           { value: 3, label: 'Mybatis' }
       ],
       selectedTagIds: [],
       /*教师数组*/
       teachers:[
           { value: 1, label: '张老师' },
           { value: 2, label: '王老师' },
           { value: 3, label: '李老师' },
           { value: 4, label: '郭老师' },
       ],
       selectedTeachers: []
   },
   methods:{
       loadTags: function () {
           $.ajax({
               url:'/portal/tag/list',
               success: function (r) {
                   let list = r.data;
                   let tags = [];
                   for (let i = 0; i < list.length; i++) {
                       let tag = {};
                       tag.value = list[i].id;
                       tag.label = list[i].name;
                       tags[i] = tag;
                   }

                   postQuestionApp.tags = tags;
               }
           })
       },
       loadTeachers: function () {
           $.ajax({
               url: '/portal/user/teacherList',
               success: function (r) {
                   let list = r.data;
                   let teachers = [];
                   for (let i = 0; i < list.length; i++) {
                       let teacher = {};
                       teacher.value = list[i].id;
                       teacher.label = list[i].nickName;
                       teachers[i] = teacher;
                   }

                   postQuestionApp.teachers = teachers;
               }
           })
       },
       postQuestion: function () {
           //alert("准备提交问题");

           let title = $('#title').val();
           let tagIds = postQuestionApp.selectedTagIds;
           let teacherIds = postQuestionApp.selectedTeachers;
           let content = $('#summernote').val();
           console.log(title);
           console.log(tagIds);
           console.log(teacherIds);
           console.log(content);

           $.ajax({
             url:'/portal/question/post',
             type: 'post',
             data:{
                 title: title,
                 content: content,
                 tagIds: tagIds,
                 teacherIds: teacherIds
             },
             traditional: true,
               success: function (r) {
                 if (r===200){
                     alert("发布问题成功")
                 }else {
                     alert(r.message)
                 }
               },
               error: function () {
                   alert('程序运行出错，请打开浏览器的控制台检查错误，如果提示500错误，还需要检查服务器端的控制台！');
               }


           })
       }
   },
    created: function () {
       this.loadTags();
       this.loadTeachers();
    }
});